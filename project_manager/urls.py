from django.urls import path
from .views import project_manager

urlpatterns = [
    path('', project_manager, name="project_manager"),
]

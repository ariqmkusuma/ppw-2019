from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.utils import timezone
from .models import Project
from .forms import ProjectForm

# Create your views here.
def project_manager(request):

    form = ProjectForm(request.POST)
    list_of_projects = Project.objects.order_by("start_date")
    
    if form.is_valid():
        name = form.cleaned_data['name']
        start_date = form.cleaned_data['start_date']
        end_date = form.cleaned_data['end_date']
        place = form.cleaned_data['place']
        project_type = form.cleaned_data['project_type']
        description = form.cleaned_data['description']

        p = Project.objects.create(
            name=name,
            start_date=start_date,
            end_date=end_date,
            description=description,
            place=place,
            project_type=project_type,
        )
        return HttpResponseRedirect("/projects/")

    response = {
        'page' : 'Project Manager',
        'date' : timezone.now(),
        'list_of_projects' : list_of_projects,
        'form' : form,
    }
    return render(request, "manager.html", response)
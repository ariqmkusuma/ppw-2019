from django import forms

class ProjectForm(forms.Form):

    name = forms.CharField(max_length=180, required=True)
    start_date = forms.DateField(required=True, label="Start date (MM/DD/YYYY)")
    end_date = forms.DateField(required=True, label="End date (MM/DD/YYYY)")
    place = forms.CharField(max_length=100, required=True)
    project_type = forms.CharField(max_length=32, required=True)
    description = forms.CharField(required=True)
from django.db import models

# Create your models here.
class Project(models.Model):

    name = models.CharField(max_length=180)
    start_date = models.DateField()
    end_date = models.DateField()
    description = models.TextField()
    place = models.CharField(max_length=100)
    project_type = models.CharField(max_length=32)

    def __str__(self):

        return self.name
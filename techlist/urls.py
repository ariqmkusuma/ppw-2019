from django.urls import path, include
from .views import techlist, get_json

urlpatterns = [
    path("", techlist, name="techlist"),
    path("json/", get_json, name="get_json"),
]

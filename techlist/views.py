from django.shortcuts import render
from django.http import JsonResponse
import json
import urllib.request

# Create your views here.
def techlist(request):
    return render(request, "techlist.html")

def get_json(request):

    url = "https://enterkomputer.com/api/product/notebook.json"
    req = urllib.request.Request(url)

    r = urllib.request.urlopen(req).read()
    json_dict = json.loads(r.decode("utf-8"))

    return JsonResponse(json_dict, safe=False)


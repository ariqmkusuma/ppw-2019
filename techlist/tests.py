from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.test import Client
from selenium import webdriver
import time

# Create your tests here.
class TechListTest(StaticLiveServerTestCase):

    def setUp(self):
        self.server = Client().get("/techlist/")
        self.browser = webdriver.Firefox()
        
        self.browser.get("%s/techlist/" % self.live_server_url)
    
    def tearDown(self):
        self.browser.quit()

    def test00_webpage_correct(self):
        self.assertEqual(self.server.status_code, 200)
        self.assertIn("Tech List", self.browser.title)
        self.assertTemplateUsed("techlist.html")
    
    def test01_list_displayed(self):
        try:
            tech = self.browser.find_element_by_class_name("tech-display")
        except:
            tech = None

        time.sleep(3)
        self.assertIsNotNone(tech)
    
    def test02_wishlist_working(self):
        try:
            wishlist = self.browser.find_element_by_id("wish-0")
        except:
            wishlist = None

        try:
            alt_wishlist = self.browser.find_element_by_id("wish-3")
        except:
            alt_wishlist = None

        self.assertIsNotNone(wishlist)
        self.assertIsNotNone(alt_wishlist)

        try:
            wishlist.click()
            alt_wishlist.click()
            price = int(self.browser.find_element_by_id("price-0").text())
            alt_price = int(self.browser.find_element_by_id("price-3").text())
            final_price = int(self.browser.find_element_by_id("total-price").text())
            click_flag = False
        except:
            click_flag = True
        
        self.assertFalse(click_flag)
        
        if (not click_flag):
            self.assertEqual(price + alt_price, final_price)

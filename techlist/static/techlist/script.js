$(function() {

        n = 0;
        x = 1;
        total = 0;
        totalPrice = 0;
        wishlist = [];

        initialize();
        loadPage();

        $("#inc").click(function() {
            if (x < total / 10) {
                n += 10;
                x++;
                loadPage();
            }
        });

        $("#dec").click(function() {
            if (x > 1) {
                n -= 10;
                x--;
                loadPage();
            }
        });

        function initialize() {
            $.getJSON("/techlist/json/"), function(result) {
                $.each(result, function(x, item) {
                    wishlist.push(false);
                })
            }
        };

        // function setPrice() {
        //     $("#total-price").empty();
        //     $("#total-price").append(totalPrice);
        // };

        function loadPage() {
            $.getJSON("/techlist/json/", function(result) {
                console.log(result);
                $("#list-wrapper").empty();
                total = 0;
                $.each(result, function(x, item) {
                    if(x <= n + 9 && x >= n + 0) {
                        $("#list-wrapper").append(
                            "<div class='tech-display' id='disp-" + x
                            + "'><h3 id='name-" + x + "'>" + item.name
                            + "</h3><h4 id='price-" + x + "'>" + item.price
                            + "</h4><p id='desc-" + x + "'>" + item.brand_description
                            + ", " + item.subcategory_description
                            + "</p><button class='wishlist' id='wish-'" + x + "'>Add To Wishlist</button></div>"
                        );

                        // $("#wish-" + x).click(function() {
                        //     if(wishlist[x]) {
                        //         totalPrice -= item.price;
                        //         wishlist[x] = false;
                        //     } else {
                        //         totalPrice += item.price;
                        //         wishlist[x] = true;
                        //     }
                        //     setPrice();
                        // })
                    }
                    total++;
                });
            });
        };

});


from django.shortcuts import render
from .forms import StatusForm
from .models import Status
# Create your views here.
def status_page(request):

    form = StatusForm(request.POST)
    list_of_status = Status.objects.order_by("publish")

    if form.is_valid():
        status = form.cleaned_data["status"]
        Status.objects.create(status=status)
    
    response = {
        'form' : form,
        'list_of_status' : list_of_status,
        'title' : 'Status: How Are You Today?'
    }
    return render(request, "status.html", response)
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.test import Client
from .models import Status
from .forms import StatusForm
from django.utils import timezone
from selenium import webdriver
import time

# Create your tests here.
class StatusTest(StaticLiveServerTestCase):

    def setUp(self):
        Status.objects.create(status="Direct model test", publish=timezone.now())
        self.client = Client()
        self.server = self.client.get("/status/")
        self.browser = webdriver.Firefox()
        self.browser.get("%s/status/" % self.live_server_url)

    def tearDown(self):
        self.browser.quit()

    def refresh(self):
        self.server = self.client.get("/status/")
        self.browser.get("%s/status/" % self.live_server_url)
    
    def test00_server_exists(self):
        self.assertEqual(self.server.status_code, 200)
        self.assertIn("Status", self.browser.title)

    def test01_template_correct(self):
        self.assertTemplateUsed(self.server, "status.html")
    
    def test02_status_submitted(self):
        tested = Status.objects.get(status="Direct model test")
        self.assertEqual(tested.status, "Direct model test")
    
    def test03_form_invalid_if_empty(self):
        form = StatusForm(data={'status' : ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['status'], ['This field is required.'])

    def test04_status_submitted_via_form(self):
        status = "Form submission test"
        self.client.post('/status/', {'status' : status})
        statusData = Status.objects.get(status=status)
        self.assertIsNotNone(statusData)
        
        self.refresh()

        time.sleep(3)

        decode = self.server.content.decode('utf8')
        self.assertIn(status, decode)
    
    def test05_status_user_submitable(self):
        status = "External submission test"
        
        form = self.browser.find_element_by_id("id_status")
        form.send_keys(status)

        time.sleep(3)

        submit = self.browser.find_element_by_id("submit")
        submit.click()
        
        self.refresh()

        time.sleep(3)

        decode = self.server.content.decode('utf8')
        self.assertIn(status, decode)

    def test06_header_display_is_correct(self):
        header = self.browser.find_element_by_tag_name('header')

        header_height = header.value_of_css_property('height')
        header_color = header.value_of_css_property('background-color')

        self.assertEqual(header_height, '60px')
        self.assertEqual(header_color, 'rgb(50, 129, 255)')

    def test07_pad_display_is_correct(self):
        pad = self.browser.find_element_by_class_name('pad')

        pad_color = pad.value_of_css_property('background-color')
        
        self.assertEqual(pad_color, 'rgb(239, 239, 239)')
    
    def test08_check_if_header_is_available(self):
        header = self.browser.find_element_by_tag_name('header')

        self.assertIsNotNone(header)
    
    def test09_check_if_form_is_available(self):
        form = self.browser.find_element_by_id('id_status')

        self.assertIsNotNone(form)
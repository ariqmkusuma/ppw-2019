from django.urls import path, include
from .views import status_page

urlpatterns = [
    path('', status_page, name="status_page"),
    path('about/', include('status_about.urls')),
]

from django.shortcuts import render
from django.utils import timezone

# Create your views here.
def profile_page(request):
    response = {
        'page' : 'Profile',
        'date' : timezone.now()
    }
    return render(request, 'profile.html', response)


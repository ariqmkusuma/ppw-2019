from django.shortcuts import render
from django.utils import timezone
from project_manager.models import Project

# Create your views here.
def projects_page(request):
    
    list_of_projects = Project.objects.order_by("start_date")

    response = {
        'page' : 'Projects',
        'date' : timezone.now(),
        'list_of_projects' : list_of_projects
    }
    return render(request, 'projects.html', response)

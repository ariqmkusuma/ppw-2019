#!/bin/bash

if [ "$1" = "exectest.sh" ]; then
    TEST_APP="$2/*"
else
    TEST_APP="$1/*"
fi

if [ -z "$1" ] || [ "$1" = "exectest.sh" ]; then {
        echo "Please include the app name."
        echo "Usage: ./exectest.sh <APPNAME>"
        echo "or bash exectest.sh <APPNAME>."
}
else {
    coverage run --include "$TEST_APP" manage.py test "$1"
    coverage report -m
}
fi

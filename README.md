**Coverage Test Status App:**

```
Name                                Stmts   Miss  Cover   Missing
-----------------------------------------------------------------
status/__init__.py                      0      0   100%
status/admin.py                         1      0   100%
status/forms.py                         3      0   100%
status/migrations/0001_initial.py       6      0   100%
status/migrations/__init__.py           0      0   100%
status/models.py                        5      0   100%
status/tests.py                        28      0   100%
status/urls.py                          3      0   100%
status/views.py                        11      0   100%
-----------------------------------------------------------------
TOTAL                                  57      0   100%
```

**Coverage Test Status About App:**

```
Name                                  Stmts   Miss  Cover   Missing
-------------------------------------------------------------------
status_about/__init__.py                  0      0   100%
status_about/admin.py                     1      0   100%
status_about/migrations/__init__.py       0      0   100%
status_about/models.py                    1      0   100%
status_about/tests.py                    13      0   100%
status_about/urls.py                      3      0   100%
status_about/views.py                     4      0   100%
-------------------------------------------------------------------
TOTAL                                    22      0   100%
```
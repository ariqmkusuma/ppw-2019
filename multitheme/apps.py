from django.apps import AppConfig


class MultithemeConfig(AppConfig):
    name = 'multitheme'

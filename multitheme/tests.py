from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from django.test import Client
import time

# Create your tests here.
class MultiThemeTest(StaticLiveServerTestCase):

    def setUp(self):
        self.server = Client().get("/multitheme/")
        self.browser = webdriver.Firefox()
        self.browser.get("%s/multitheme/" % self.live_server_url)

    def tearDown(self):
        self.browser.quit()

    def test00_webpage_correct(self):
        time.sleep(3)
        self.assertEqual(self.server.status_code, 200)
        self.assertTemplateUsed(self.server, "multitheme.html")
        self.assertIn("Profile", self.browser.title)
    
    def test01_theme_changer_working(self):
        time.sleep(2)
        button = self.browser.find_element_by_id("dark-mode")

        button.click()
        time.sleep(2)

        dark_mode = self.browser.find_element_by_class_name("dark")
        self.assertIsNotNone(dark_mode)

        button.click()
        time.sleep(2)
        
        # this is a little special.
        try:
            dark_mode = self.browser.find_element_by_class_name("dark")
        except:
            dark_mode = None

        self.assertIsNone(dark_mode)

    def test02_accordion_working(self):
        accordion = self.browser.find_element_by_id("meta-set")

        self.assertIsNotNone(accordion)

        bio_select = self.browser.find_element_by_id("bio-select")
        proj_select = self.browser.find_element_by_id("proj-select")
        ach_select = self.browser.find_element_by_id("ach-select")

        proj_select.click()
        time.sleep(1)
        ach_select.click()
        time.sleep(1)
        bio_select.click()
        time.sleep(2)
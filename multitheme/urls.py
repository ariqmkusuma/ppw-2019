from django.urls import path, include
from .views import multitheme

urlpatterns = [
    path('', multitheme, name="multitheme"),
]
from django.urls import path
from .views import status_about

urlpatterns = [
    path('', status_about, name='status_about')
]

from django.apps import AppConfig


class StatusAboutConfig(AppConfig):
    name = 'status_about'

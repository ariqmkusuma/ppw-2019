from django.shortcuts import render

# Create your views here.
def status_about(request):

    response = {
        'title' : 'Status - About',
    }
    return render(request, "status-about.html")
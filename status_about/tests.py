from django.test import TestCase, Client

# Create your tests here.
class StatusAboutTest(TestCase):

    def setUp(self):
        self.server = Client().get('/status/about/')

    def test00_page_exists(self):
        self.assertEqual(self.server.status_code, 200)

    def test01_correct_template_used(self):
        self.assertTemplateUsed('status_about.html')

    def test02_page_contains_info(self):
        decode = self.server.content.decode('utf8')
        self.assertIn('<div class="profile">', decode)
        self.assertIn('Ariq Munif Kusuma', decode)
        self.assertIn('1706039692', decode)

from django.shortcuts import render
from django.utils import timezone
from django.http import HttpResponse
from .forms import LoginForm, RegisterForm
from .models import User

# Create your views here.
def login_register(request):
    login_form = LoginForm()
    register_form = RegisterForm()

    if(request.method == "POST"):
        posted_uname = request.POST['reg_uname']
        posted_email = request.POST['reg_email']
        posted_pass = request.POST['reg_pass']
        conf_pass = request.POST['reg_conf_pass']
        
        if(conf_pass == posted_pass):
            try:
                existing_uname = User.objects.get(username=posted_uname)
                return HttpResponse("Username has been registered")
            except:
                try:
                    existing_email = User.objects.get(email=posted_email)
                    return HttpResponse("Email has been registered")
                except:
                    User.objects.create(username=posted_uname, email=posted_email, password=posted_pass)
                    return HttpResponse("Success")

    response = {
        'page' : 'Log In / Register',
        'date' : timezone.now(),
        'register_form' : register_form
    }
    return render(request, 'register.html', response)




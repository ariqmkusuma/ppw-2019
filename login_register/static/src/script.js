$(function() {
    $("#register-form").on('submit', function(result) {
        result.preventDefault();
        $.ajax({
            type : "POST",
            url : "/register/",
            data : {
                reg_uname : $("#id_reg_uname").val(),
                reg_email : $("#id_reg_email").val(),
                reg_pass : $("#id_reg_pass").val(),
                reg_conf_pass : $("#id_reg_conf_pass").val(),
                csrfmiddlewaretoken : $("input[name=csrfmiddlewaretoken]").val()
            },
            success : function(response) {
                alert(response.message);
            }
        });
    });
});
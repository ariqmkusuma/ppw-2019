from django import forms
from .models import User

class LoginForm(forms.Form):

    login_uname = forms.CharField(min_length=8, max_length=32)
    login_pass = forms.CharField(min_length=8, max_length=64, widget=forms.PasswordInput())

class RegisterForm(forms.Form):

    reg_uname = forms.CharField(
        min_length=8,
        max_length=32,
        required=True,
        widget=forms.TextInput(attrs={'placeholder' : 'Username'})
    )
    reg_email = forms.EmailField(
        widget=forms.EmailInput(attrs={'placeholder' : 'Email'})
    )
    reg_pass = forms.CharField(
        min_length=8,
        max_length=64,
        required=True,
        widget=forms.PasswordInput(attrs={'placeholder' : 'Password'})
    )
    reg_conf_pass = forms.CharField(
        min_length=8,
        max_length=64,
        required=True,
        widget=forms.PasswordInput(attrs={'placeholder' : 'Confirm Password'})
    )
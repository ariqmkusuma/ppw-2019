$(function() {

    darkModeActive = false;
    $("#profile-dark").hide();

    $("#meta-set").accordion({
        collapsible: true
    });
    
    $("#dark-mode").click(function() {
        if(darkModeActive) {
            $("*").removeClass("dark");
            $("#profile-dark").hide();
            $("#profile").show();
            darkModeActive = false;
        } else {
            $("*").addClass("dark");
            $("#profile-dark").show();
            $("#profile").hide();
            darkModeActive = true;
        }
    });
    
});